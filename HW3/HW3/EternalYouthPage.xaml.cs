﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;


namespace HW3
{
	public partial class EternalYouthPage : ContentPage
	{
		public EternalYouthPage (string hello)
        {
            InitializeComponent();

            greeting.Text = hello;
        }

    }
}