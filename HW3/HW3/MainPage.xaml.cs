﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW3
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
            
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        async void OnEternalYouthTapped(object sender, EventArgs e)
        {
             
                await Navigation.PushAsync(new EternalYouthPage("You made it"));
            
        }
        async void OnDeathTapped(object sender, EventArgs e)
        {
            

            bool usersResponse = await DisplayAlert("To Death",
                         "Are you really sure you want to continue?",
                         "Bring it on!",
                         "Nah");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new OnDeathPage());
            }

        }
    }
}
